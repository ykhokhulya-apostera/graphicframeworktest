#!/bin/sh

set -e

base_dir=$(dirname $(readlink -f $0))

conan install \
    --install-folder $base_dir/build/host-build \
    $base_dir

cmake \
    -S $base_dir \
    -B $base_dir/build/host-build \
    -DCMAKE_INSTALL_PREFIX=$base_dir/build/host-install

cmake \
    --build $base_dir/build/host-build \
    --parallel $(nproc) \
    --target $@