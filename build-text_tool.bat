set VC_TOOL_2017_VARS="C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\VC\\Auxiliary\\Build\\vcvarsall.bat"
set QT_TOOL_2008_VARS="C:\\Qt\\4.8.4\\bin\\qtvars.bat"

if not exist %VC_TOOL_2017_VARS% goto missing
if not exist %QT_TOOL_2008_VARS% goto missing

@REM Build font library using MSVC 2017
Setlocal

call %VC_TOOL_2017_VARS% x86

cmake -S . -B build/text_tool-build -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build build/text_tool-build
cmake --install build/text_tool-build --component TextTool --prefix build/text_tool-install

EndLocal

REM @REM Buil text tool plugin using MSVC 2008
REM Setlocal
REM call %QT_TOOL_2008_VARS% vsvars
REM
REM cd text_tool_plugin/qt4_project
REM qmake build_all.pro
REM nmake BUILD=release
REM nmake install
REM
REM EndLocal

goto finish

:missing
echo The specified configuration type is missing.  The tools for the
echo configuration might not be installed.

:finish
