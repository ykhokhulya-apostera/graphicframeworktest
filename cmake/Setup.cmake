###############################################################################
# Project settings
###############################################################################

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED off)
set(CMAKE_CXX_EXTENSIONS off)

set(CMAKE_POSITION_INDEPENDENT_CODE TRUE)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/modules)

if(BUILD_UNITTESTS)
    enable_testing()
endif()

###############################################################################
# Third-parties
###############################################################################

set(Boost_NO_WARN_NEW_VERSIONS 1)

if(MSVC)
    list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/glm)
    list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/gtest)
    list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/graphics)
	list(APPEND CMAKE_PREFIX_PATH ${CMAKE_SOURCE_DIR}/third-parties/msvc/stb_image)
    set(ENV{FREETYPE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/freetype)
    set(BOOST_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/boost)
    set(Boost_NO_BOOST_CMAKE ON)
    set(Boost_USE_STATIC_LIBS ON)
    set(Boost_USE_STATIC_RUNTIME ON)

    # fix build of boost related errors
    add_definitions(-DBOOST_RANGE_ENABLE_CONCEPT_ASSERT=0)
    link_directories(${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/boost/lib)

    add_definitions(-D_CRT_SECURE_NO_WARNINGS)
    add_definitions(-D__PRETTY_FUNCTION__=__FUNCSIG__)

    set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
    if(True)
         # Modify compile flags to change MSVC runtime from /MD to /MT
         set(_re_match "([\\/\\-]M)D")
         set(_re_replace "\\1T")
         string(REGEX REPLACE ${_re_match} ${_re_replace}
           CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
         string(REGEX REPLACE ${_re_match} ${_re_replace}
           CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
         string(REGEX REPLACE ${_re_match} ${_re_replace}
           CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
         string(REGEX REPLACE ${_re_match} ${_re_replace}
           CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL}")
         string(REGEX REPLACE ${_re_match} ${_re_replace}
           CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
    endif()
else()
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup(TARGETS)
    list(APPEND CMAKE_PREFIX_PATH ${CMAKE_CURRENT_SOURCE_DIR}/third-parties/msvc/glm) # TODO
endif()

###############################################################################
# Helper functions (stubs)
###############################################################################

include(CMakeDependentOption)
include(CheckCXXCompilerFlag)

function(get_repo_version REPO_DIR OUT_VERSION_VAR)
    set(${OUT_VERSION_VAR} 0.0.0 PARENT_SCOPE)
endfunction()

function(documentation_generate_adoc)
endfunction()

function(target_attach_version_info)
endfunction()

function(install_shaders)
endfunction()

function(add_sw_unit_tests TARGET_NAME)
    GTEST_ADD_TESTS(${TARGET_NAME} "" AUTO)
endfunction()

function(use_ipc_api)
endfunction()

function(use_ipc_api_include_directories)
endfunction()

function(use_ipc_api_for_shared)
endfunction()

function(enable_cxx_compiler_flag_if_supported FLAG)
    if(MSVC)
        return()
    endif()

    string(FIND "${CMAKE_CXX_FLAGS}" "${FLAG}" FLAG_ALREDY_SET)
    if(FLAG_ALREDY_SET EQUAL -1)
        set(FLAG_TO_TEST "test_${FLAG}_cxx_flag")
        string(REPLACE "=" "_" FLAG_TO_TEST ${FLAG_TO_TEST})
        if(NOT DEFINED ${FLAG_TO_TEST})
            check_cxx_compiler_flag("${FLAG}" ${FLAG_TO_TEST})
            if (${FLAG_TO_TEST})
                set(${FLAG_TO_TEST} ${${FLAG_TO_TEST}} CACHE BOOL "Supported CXX compiler flag" FORCE)
            else()
                set(${FLAG_TO_TEST} ${${FLAG_TO_TEST}} CACHE BOOL "Unsupported CXX compiler flag" FORCE)
            endif()
        endif()
        if(${FLAG_TO_TEST})
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${FLAG}" PARENT_SCOPE)
        endif()
    endif()
endfunction()

function(check_outer_project_target TARGET_VAR TARGET_NAME)
    if(NOT TARGET ${TARGET_NAME})
        message(FATAL_ERROR "Target ${TARGET_NAME} is missing in the outer project.")
    endif()
    set(${TARGET_VAR} ${TARGET_NAME} PARENT_SCOPE)
endfunction()
