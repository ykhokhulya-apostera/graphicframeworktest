# Find EGL library.
#
# This module defines:
#
#  EGL_FOUND            - True if EGL library is found
#  EGL::EGL             - EGL imported target
#
# Additionally these variables are defined for internal usage:
#
#  EGL_LIBRARY          - EGL library
#  EGL_INCLUDE_DIR      - Include dir
#

if(WIN32)
    find_library(EGL_LIBRARY NAMES libEGL)
else()
    find_library(EGL_LIBRARY NAMES EGL)
endif()

find_path(EGL_INCLUDE_DIR NAMES EGL/egl.h)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(EGL DEFAULT_MSG
    EGL_LIBRARY
    EGL_INCLUDE_DIR)

if(EGL_FOUND)
    if(NOT TARGET EGL::EGL)
        add_library(EGL::EGL UNKNOWN IMPORTED)
        set_target_properties(EGL::EGL PROPERTIES
            IMPORTED_NO_SONAME TRUE
            IMPORTED_LINK_INTERFACE_LANGUAGES "C"
            INTERFACE_INCLUDE_DIRECTORIES ${EGL_INCLUDE_DIR}
            IMPORTED_LOCATION ${EGL_LIBRARY})
    endif()
endif()
