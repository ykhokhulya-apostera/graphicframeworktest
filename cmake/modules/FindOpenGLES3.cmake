# Find OpenGL ES 3 library.
#
# This module defines:
#
#  OpenGLES3_FOUND          - True if OpenGL ES 3 library is found
#  OpenGLES3::OpenGLES3     - OpenGL ES 3 imported target
#
# Additionally these variables are defined for internal usage:
#
#  OPENGLES3_LIBRARY        - OpenGL ES 3 library
#  OPENGLES3_INCLUDE_DIR    - Include dir
#

if(WIN32)
    find_library(OPENGLES3_LIBRARY NAMES libGLESv2)
elseif(ANDROID)
    find_library(OPENGLES3_LIBRARY NAMES GLESv3)
else()
    find_library(OPENGLES3_LIBRARY NAMES GLESv2 )
endif()

find_path(OPENGLES3_INCLUDE_DIR NAMES GLES3/gl3.h)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(OpenGLES3 DEFAULT_MSG
    OPENGLES3_LIBRARY
    OPENGLES3_INCLUDE_DIR)

if(OpenGLES3_FOUND)
    if(NOT TARGET OpenGLES3::OpenGLES3)
        add_library(OpenGLES3::OpenGLES3 UNKNOWN IMPORTED)
        set_target_properties(OpenGLES3::OpenGLES3 PROPERTIES
            IMPORTED_NO_SONAME TRUE
            IMPORTED_LINK_INTERFACE_LANGUAGES "C"
            INTERFACE_INCLUDE_DIRECTORIES ${OPENGLES3_INCLUDE_DIR}
			INTERFACE_COMPILE_DEFINITIONS "GL_GLEXT_PROTOTYPES"
            IMPORTED_LOCATION ${OPENGLES3_LIBRARY})
    endif()
endif()
