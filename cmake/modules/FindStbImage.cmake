# This module defines:
#
#  StbImage_FOUND            - True if StbImage library is found
#  StbImage::StbImage         - StbImage imported target
#
# Additionally this variable is defined for internal usage:
#
#  StbImage_INCLUDE_DIR      - Include dir
#

file(GLOB StbImage_SEARCH_PATHS
    ${CMAKE_BINARY_DIR}/deps/
    ${CMAKE_BINARY_DIR}/deps/stb)
find_path(StbImage_INCLUDE_DIR
    NAMES stb/stb_image.h stb/StbImage_write.h
    PATHS ${StbImage_SEARCH_PATHS})

if(StbImage_INCLUDE_DIR)
  set(StbImage_FOUND YES)
else ()
  find_path(StbImage_INCLUDE_DIR
        NAMES stb_image.h StbImage_write.h
        PATHS ${StbImage_SEARCH_PATHS})
  if(StbImage_INCLUDE_DIR)
    set(StbImage_FOUND YES)
  endif ()
endif ()

# Make sure that stb/ is the last part of the include directory, if it was
# found.
if (StbImage_INCLUDE_DIR)
  string(REGEX MATCH ".*stb[/]?" STB_INCLUDE_HAS_TRAILING_STB
      "${StbImage_INCLUDE_DIR}")
  if (NOT STB_INCLUDE_HAS_TRAILING_STB)
    set(StbImage_INCLUDE_DIR "${StbImage_INCLUDE_DIR}/stb/")
  endif ()
endif ()

# Checks 'REQUIRED'.
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(StbImage
    REQUIRED_VARS StbImage_INCLUDE_DIR)

mark_as_advanced(StbImage_INCLUDE_DIR)

if(StbImage_FOUND)
    if(NOT TARGET StbImage::StbImage)
        add_library(StbImage::StbImage INTERFACE IMPORTED)
        set_target_properties(StbImage::StbImage PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${StbImage_INCLUDE_DIR}")
    endif()
endif()
