#pragma once

#include "elogger/Options.hpp"

namespace ELogger {

struct AppInfo
{
    AppInfo(std::string, std::string){}
};

struct ContextInfo
{
    ContextInfo(std::string, std::string){}
};

struct Config
{
    Config(AppInfo, ContextInfo, Options){}
};

inline void init(const Config&)
{
}

} // namespace ELogger