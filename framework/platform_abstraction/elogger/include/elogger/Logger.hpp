#pragma once

#include "elogger/Config.hpp"

#define ELOG_DECLARE_EXTERNAL_NAMED_SCOPE(X) extern std::string c_##X;
#define ELOG_DECLARE_NAMED_SCOPE(X) std::string c_##X = #X;

#define ELOG_USE_NAMED_SCOPE(X) static std::string elog_scope = c_##X;

#ifndef ANDROID

#include <iostream>

// clang-format off
#define ELOG_TRC std::cout << std::endl << "TRC [" << elog_scope << "] "
#define ELOG_INF std::cout << std::endl << "INF [" << elog_scope << "] "
#define ELOG_ERR std::cout << std::endl << "ERR [" << elog_scope << "] "
#define ELOG_FAT std::cout << std::endl << "FAT [" << elog_scope << "] "
#define ELOG_DBG std::cout << std::endl << "DBG [" << elog_scope << "] "
#define ELOG_WRN std::cout << std::endl << "WRN [" << elog_scope << "] "
#define ELOG_WRN_IF(X) if (X) ELOG_WRN
// clang-format on

#else

#include <sstream>

#include <android/log.h>

namespace Logger {

class LogStream
{
public:
    LogStream(int log_level, const char* scope_name)
        : m_log_level(log_level)
        , m_scope_name(scope_name)
    {
    }

    ~LogStream()
    {
        __android_log_print(
            m_log_level, m_scope_name, "%s", m_stream.str().c_str());
    }

    template <typename T>
    LogStream& operator<<(T const& value)
    {
        m_stream << value;
        return *this;
    }

private:
    std::stringstream m_stream;
    int m_log_level;
    const char* m_scope_name;
};

} // namespace Logger

// clang-format off
#define ELOG_TRC Logger::LogStream(ANDROID_LOG_VERBOSE, elog_scope.c_str())
#define ELOG_INF Logger::LogStream(ANDROID_LOG_INFO, elog_scope.c_str())
#define ELOG_ERR Logger::LogStream(ANDROID_LOG_ERROR, elog_scope.c_str())
#define ELOG_FAT Logger::LogStream(ANDROID_LOG_FATAL, elog_scope.c_str())
#define ELOG_DBG Logger::LogStream(ANDROID_LOG_DEBUG, elog_scope.c_str())
#define ELOG_WRN Logger::LogStream(ANDROID_LOG_WARN, elog_scope.c_str())
#define ELOG_WRN_IF(X) if (X) ELOG_WRN
// clang-format on

#endif