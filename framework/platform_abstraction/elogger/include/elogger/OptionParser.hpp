#pragma once

#include <boost/program_options.hpp>

#include "elogger/Options.hpp"

namespace ELogger {

inline void addOptions(boost::program_options::options_description&, Options&) {}

} // namespace ELogger
