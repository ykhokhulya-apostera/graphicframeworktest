#pragma once

#include <string>
#include <vector>

namespace ELogger {

enum class ESeverityLevel
{
    Trace,
    Debug,
    Info,
    Warning,
    Error,
    Fatal,

    Total
};

enum class ELogDestination
{
    None,
    File,
    Console,
    Dlt,

    Total
};

struct Options
{
    ESeverityLevel severity;
    std::vector<ELogDestination> destination_list;
    std::string log_filename;
    std::string log_directory;
};

} // namespace ELogger