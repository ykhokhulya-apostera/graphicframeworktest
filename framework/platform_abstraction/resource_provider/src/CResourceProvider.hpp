#ifndef HEADER_GUARD_GQITLLRUXQKQJCIM
#define HEADER_GUARD_GQITLLRUXQKQJCIM

#include "resource_provider/IResourceProvider.hpp"

namespace ResourceProvider {

class CResourceProvider : public IResourceProvider
{
public: // methods
    RequestId requestResource(ResourceParameters const& params, OnResultFn on_result) override
    {
        return c_invalid_request_id;
    }

    RequestId requestIconResource(
        ResourceParameters const& params,
        OnImageResultFn on_result) override
    {
        return c_invalid_request_id;
    }

    void cancelRequest(RequestId /*request_id*/) override {}

    void cancelAllRequests() override {}
};

} // namespace ResourceProvider

#endif // HEADER_GUARD_GQITLLRUXQKQJCIM
