
#include "resource_provider/ResourceProvider.hpp"

#include "CResourceProvider.hpp"

namespace ResourceProvider {

ResourceProviderUPtr createInstance()
{
    return std::make_unique<CResourceProvider>();
}

} // namespace ResourceProvider