#include <cstdlib>
#include <iostream>
#include <numeric>
#include <sstream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <glm/gtx/io.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/vector_relational.hpp>

#include "bezier_lib/BezierLib.hpp"
#include "bezier_lib/CubicBezierCurveIterator.hpp"
#include "display_control/DisplayControl.hpp"
#include "display_control/events/CKeyReleasedEvent.hpp"
#include "elogger/Logger.hpp"
#include "elogger/OptionParser.hpp"
#include "graphic/GraphicRoot.hpp"
#include "graphic/GraphicSettingsParser.hpp"
#include "graphic/math/Vector.hpp"
#include "graphic/scene/CMeshNode.hpp"
#include "graphic/scene/CTextNode.hpp"

using namespace Graphic;
using namespace Utility;

namespace {

const glm::vec2 c_wnd_size = {1053, 351};
const DisplayControl::SurfaceFormat c_surface_format = {8, 8, 8, 8, 24, 8, 2, 1};

const float c_camera_fov = glm::radians(4.f);
const float c_near = 0.1f;
const float c_far = 100.f;

const std::string c_dev_resources_path =
    "/home/ykhokhulya/work/apostera/host-dev/adson_poc/test/test_carpet/resources/";

struct KeyboardListener : DisplayControl::IEventsListener
{
    void onEvent(const DisplayControl::CKeyReleasedEvent& event)
    {
        for (auto& func : funcs)
        {
            func(event);
        }
    }

    std::vector<std::function<void(const DisplayControl::CKeyReleasedEvent&)>> funcs;
};

BezierLib::CubicBezierCurvesList<float> generateCurve()
{
    static const BezierLib::CurveWithNormals<float> c_input_curve = {
        {glm::vec2{0, 0.0}, glm::normalize(glm::vec2{0, 1})},
        {glm::vec2{30, 0.5}, glm::normalize(glm::vec2{-0.11043, 0.9939})},
        {glm::vec2{100, 1.5}, glm::normalize(glm::vec2{0, 1})},
    };

    // static const BezierLib::CurveWithNormals<float> c_input_curve = {
    //     {glm::vec2{0, 0.0}, glm::normalize(glm::vec2{0, 1})},
    //     {glm::vec2{100, 0.0}, glm::normalize(glm::vec2{0, 1})},
    // };

    const BezierLib::CubicBezierCurvesList<float> bezier_curve =
        BezierLib::convertToCubicCurves(c_input_curve);

    return bezier_curve;
}

glm::vec2 calculatePosition(const BezierLib::CubicBezierCurvesList<float>& bezier_curve, const glm::vec3& pos)
{
    const auto in_pos = glm::vec2{-pos.z, -pos.x};

    BezierLib::Circle<float> circle{glm::vec2{0.f}, glm::length(in_pos)};

    BezierLib::IntersectionResultList<float> circle_intersections =
        BezierLib::calcIntersection(bezier_curve, circle);

    for (const auto& v : circle_intersections)
    {
        auto cubic_bezier = bezier_curve[v.intersection];

        const auto point = cubic_bezier.point(v.intersection);
        if (point.x > 0.f)
        {
            const auto tangent = cubic_bezier.point(v.intersection);
            const bool oriented = 0.f < glm::orientedAngle(tangent, point - in_pos);
            const float distance_to_curve = glm::distance(point, in_pos);
            return {oriented ? -distance_to_curve : distance_to_curve, 25.f};
        }
    }
    return glm::vec2{};
}

Mesh::GeometryPtr generateCarpetMeshCurve(
    Mesh::IMeshManager& mesh_manager,
    BezierLib::CubicBezierCurvesList<float> bezier_curve)
{
    auto primitive_type = Mesh::EPrimitiveType::TriangleStrip;

    const float c_carpet_length = 80.f;
    const float c_carpet_half_width = 10.f;
    const float c_z_step = 2.f;
    const float c_start_shading = 0.8f;

    const auto c_steps = static_cast<std::size_t>(c_carpet_length / c_z_step);

    Mesh::VerticesList vertices;
    vertices.reserve(c_steps * 2u);

    glm::vec2 prev = glm::vec2();
    float distance = 0.f;
    for (auto&& a : BezierLib::iterateCurveListWithStep(bezier_curve, c_z_step))
    {
        distance += glm::distance(a.point(), prev);
        prev = a.point();

        glm::vec2 p1 = a.point() + -c_carpet_half_width * a.normal();
        glm::vec2 p2 = a.point() + c_carpet_half_width * a.normal();
        float z = distance;
        float av = z / c_carpet_length;
        float ad = glm::smoothstep(c_start_shading, 1.f, av);

        // color attribute is used for
        vertices.emplace_back(
            Mesh::Vertex({p1.y, 0.f, -p1.x}, {-1.f, av, ad, 1.f}, {-c_carpet_half_width, z}));
        std::cout << vertices.back().position << std::endl;
        vertices.emplace_back(
            Mesh::Vertex({p2.y, 0.f, -p2.x}, {1.f, av, ad, 1.f}, {c_carpet_half_width, z}));
        std::cout << vertices.back().position << std::endl;
    }

    Mesh::IndicesList indices;
    indices.resize(vertices.size());
    std::iota(indices.begin(), indices.end(), 0);

    auto mat = std::make_shared<Mesh::CMaterial>();
    mat->getDepthCheck().enabled = false;
    auto geom = mesh_manager.createGeometry(vertices, indices, primitive_type);

    return mesh_manager.createGeometry(vertices, indices, primitive_type);
}

Mesh::GeometryPtr generateCarpetMesh(Mesh::IMeshManager& mesh_manager)
{
    auto primitive_type = Mesh::EPrimitiveType::TriangleStrip;

    const float c_carpet_length = 100.f;
    const float c_carpet_half_width = 5.f;
    const float c_z_step = 4.f;
    const float c_start_shading = 0.8f;

    const auto c_steps = static_cast<std::size_t>(c_carpet_length / c_z_step);

    Mesh::VerticesList vertices;
    vertices.reserve(c_steps * 2u);

    for (std::size_t i = 0u; i < c_steps; ++i)
    {
        float x1 = -c_carpet_half_width;
        float x2 = c_carpet_half_width;
        float z = i * c_z_step;
        float av = z / c_carpet_length;
        float ad = glm::smoothstep(c_start_shading, 1.f, av);

        // color attribute is used for
        vertices.emplace_back(Mesh::Vertex({x1, 0.f, -z}, {-1.f, av, ad, 1.f}, {x1, z}));
        vertices.emplace_back(Mesh::Vertex({x2, 0.f, -z}, {1.f, av, ad, 1.f}, {x2, z}));
    }

    Mesh::IndicesList indices;
    indices.resize(vertices.size());
    std::iota(indices.begin(), indices.end(), 0);

    auto mat = std::make_shared<Mesh::CMaterial>();
    mat->getDepthCheck().enabled = false;
    auto geom = mesh_manager.createGeometry(vertices, indices, primitive_type);

    return mesh_manager.createGeometry(vertices, indices, primitive_type);
}

inline glm::mat3 calcMaskUVPosition(const glm::vec2& mask_pos, const glm::vec2& mask_size)
{
    const auto size = Math::Vector::maxComponents(mask_size, glm::vec2{0.1f});
    const auto inv_size = glm::vec2{1.f / size.x, -1.f / size.y};

    /// Use optimized version for texture matrix calculation:
    ///
    /// @code
    /// glm::mat3 tex_mat;
    /// tex_mat = glm::scale(tex_mat, inv_size);
    /// tex_mat = glm::translate(tex_mat, -mask_pos + size * 0.5f);
    /// @endcode

    glm::mat3 tex_mat(
        glm::vec3{inv_size.x, 0.f, 0.f},
        glm::vec3{0.f, inv_size.y, 0.f},
        glm::vec3{-mask_pos * inv_size + glm::vec2{0.5f, 0.f}, 1.f});

    return tex_mat;
}

Scene::ScenePtr createScene(IGraphicRoot& graphic, KeyboardListener& keyboard_listener)
{
    auto& graphic_context = graphic.getGraphicContext();
    auto& scene_manager = graphic.getSceneManager();
    auto& mesh_manager = graphic.getMeshManager();
    auto& texture_manager = graphic.getTextureManager();

    auto scene = scene_manager.createScene();

    /// virtual scene camera setup
    auto camera = Scene::CCameraNode::create();
    camera->setFrustum(Camera::CFrustum(c_wnd_size.x / c_wnd_size.y, c_camera_fov, c_near, c_far));
    camera->setRotation(glm::quat({glm::radians(-2.f), glm::radians(0.67f), 0.f}));
    camera->setPosition({0.f, 1.33f, 0.f});
    scene->setActiveCamera(camera);
    scene->addChild(camera);

    keyboard_listener.funcs.push_back([camera](auto event) {
        static float yaw = glm::radians(0.67f);
        switch (event.getKeyCode())
        {
        case DisplayControl::EKeyCode::KeyW:
            camera->setRotation(glm::quat({glm::radians(-2.f), yaw, 0.f}));
            break;
        case DisplayControl::EKeyCode::KeyS:
            camera->setRotation(glm::quat({glm::radians(-2.f), yaw, 0.f}));
            break;
        case DisplayControl::EKeyCode::KeyA:
            yaw += glm::radians(0.1f);
            camera->setRotation(glm::quat({glm::radians(-2.f), yaw, 0.f}));
            break;
        case DisplayControl::EKeyCode::KeyD:
            yaw -= glm::radians(0.1f);
            camera->setRotation(glm::quat({glm::radians(-2.f), yaw, 0.f}));
            break;
        }
    });

    const BezierLib::CubicBezierCurvesList<float> bezier_curve = generateCurve();

    auto carpet_geom = generateCarpetMeshCurve(mesh_manager, bezier_curve);
    auto quad_xz = mesh_manager.createQuad(Mesh::EQuadPlane::XZ);

    auto create_dots_mat = [&graphic_context, &texture_manager](
                               const Graphic::Color& color, const std::string& mask_name) {
        auto dots_mat = std::make_shared<Mesh::CMaterial>();
        dots_mat->setType(Mesh::EMaterialType::Program);
        dots_mat->setColor(color);
        dots_mat->setDepthCheck(Mesh::DepthCheckOptionalProperty());
        dots_mat->setProgram(graphic_context.createGpuProgram("carpet/dots_single_mask"));
        dots_mat->setCustomProperty("u_cell_size", glm::vec2{0.1, 1.0});
        dots_mat->setCustomProperty("u_dot_size_coeff", 0.6f);
        dots_mat->setCustomProperty("u_mask0", texture_manager.createTexture(mask_name));

        return dots_mat;
    };

    auto create_dots_line_mat = [&graphic_context, &texture_manager](
                                    const Graphic::Color& color,
                                    const std::string& mask0_name,
                                    const std::string& mask1_name) {
        auto dots_line_mat = std::make_shared<Mesh::CMaterial>();
        dots_line_mat->setType(Mesh::EMaterialType::Program);
        dots_line_mat->setColor(color);
        dots_line_mat->setDepthCheck(Mesh::DepthCheckOptionalProperty());
        dots_line_mat->setProgram(graphic_context.createGpuProgram("carpet/dots_line_mask"));
        dots_line_mat->setCustomProperty("u_cell_size", glm::vec2{0.1, 1.0});
        dots_line_mat->setCustomProperty("u_dot_size_coeff", 0.6f);
        dots_line_mat->setCustomProperty("u_mask0", texture_manager.createTexture(mask0_name));
        dots_line_mat->setCustomProperty("u_mask1", texture_manager.createTexture(mask1_name));

        return dots_line_mat;
    };

    auto create_color_mask_mat = [&graphic_context, &texture_manager](
                                     const Graphic::Color& color, const std::string& mask_name) {
        auto color_mask_mat = std::make_shared<Mesh::CMaterial>();
        color_mask_mat->setType(Mesh::EMaterialType::Program);
        color_mask_mat->setColor(color);
        color_mask_mat->setDepthCheck(Mesh::DepthCheckOptionalProperty());
        color_mask_mat->setProgram(graphic_context.createGpuProgram("carpet/color_mask"));
        color_mask_mat->setTexture(texture_manager.createTexture(mask_name));

        return color_mask_mat;
    };

    /// aca activation
    {
        const auto color = Color::fromHex(0xffffffff);
        const auto pos = glm::vec2{0.f, 80.f};

        const auto lane_width = 3.4f;
        const auto lane_length = 80.f;

        auto dots_mat = create_dots_mat(color, "carpet/aca_activation");
        auto dots_mask_mesh = mesh_manager.createMesh(carpet_geom, dots_mat);

        auto lane_node = Scene::CMeshNode::create(dots_mask_mesh);
        lane_node->setOrder(-128);
        lane_node->getMaterial().setCustomProperty(
            "u_mask0_tex_mat", calcMaskUVPosition(pos, {lane_width, lane_length}));
        scene->addChild(lane_node);

        keyboard_listener.funcs.push_back([lane_node](auto event) {
            static float offset = 0.f;
            switch (event.getKeyCode())
            {
            case DisplayControl::EKeyCode::Key4:
                lane_node->setVisible(!lane_node->isVisible());
                break;
            }
        });
    }

    /// detected vehicle
    {
        const auto color = Color::fromHex(0x00ff00ff);
        const auto pos = glm::vec3{0.f, 0.f, -25.5f};
        const auto line_width = 2.5f;
        const auto line_thickness = 1.f;
        const auto tail_thickness = 5.f;
        const auto line_gap = 0.1f;

        const auto line_pos_offset = glm::vec2{};
        const auto line_size = glm::vec2{line_width, line_thickness};
        const auto dots_line_offset = glm::vec2{0.f, -line_thickness - line_gap};
        const auto dots_line_size = glm::vec2{line_width, tail_thickness};

        const auto carpet_pos = calculatePosition(bezier_curve, pos);

        auto dots_line_mat = create_dots_line_mat(color, "carpet/acc_level", "carpet/active_line");
        auto dots_line_mask_mesh = mesh_manager.createMesh(carpet_geom, dots_line_mat);

        auto line_tail_node = Scene::CMeshNode::create(dots_line_mask_mesh);
        line_tail_node->setOrder(-32);
        line_tail_node->getMaterial().setCustomProperty(
            "u_mask1_tex_mat", calcMaskUVPosition(carpet_pos + line_pos_offset, line_size));
        line_tail_node->getMaterial().setCustomProperty(
            "u_mask0_tex_mat", calcMaskUVPosition(carpet_pos + dots_line_offset, dots_line_size));
        scene->addChild(line_tail_node);

        keyboard_listener.funcs.push_back(
            [line_tail_node, line_pos_offset, dots_line_offset, line_size, dots_line_size, carpet_pos](
                auto event) {
                static glm::vec2 pos = carpet_pos;
                static bool updated = false;
                switch (event.getKeyCode())
                {
                case DisplayControl::EKeyCode::Key1:
                    line_tail_node->setVisible(!line_tail_node->isVisible());
                    break;
                case DisplayControl::EKeyCode::KeyC:
                    pos.x -= 0.25f;
                    updated = true;
                    break;
                case DisplayControl::EKeyCode::KeyB:
                    pos.x += 0.25f;
                    updated = true;
                    break;
                case DisplayControl::EKeyCode::KeyV:
                    pos.y -= 0.25f;
                    updated = true;
                    break;
                case DisplayControl::EKeyCode::KeyG:
                    pos.y += 0.25f;
                    updated = true;
                    break;
                }
                if (updated)
                {
                    line_tail_node->getMaterial().setCustomProperty(
                        "u_mask1_tex_mat",
                        calcMaskUVPosition(pos + line_pos_offset, line_size));
                    line_tail_node->getMaterial().setCustomProperty(
                        "u_mask0_tex_mat",
                        calcMaskUVPosition(pos + dots_line_offset, dots_line_size));

                    updated = false;
                }
            });
    }

    /// left line
    {
        const auto color = Color::fromHex(0xff0000ff);
        const auto pos = glm::vec2{1.47f, 50.f};
        const auto size = glm::vec2{0.4f, 50.f};

        auto dots_mat = create_dots_mat(color, "carpet/ldw");
        auto dots_mask_mesh = mesh_manager.createMesh(carpet_geom, dots_mat);

        auto line_node = Scene::CMeshNode::create(dots_mask_mesh);
        line_node->getMaterial().setCustomProperty("u_mask0_tex_mat", calcMaskUVPosition(pos, size));
        line_node->setOrder(-64);
        scene->addChild(line_node);

        keyboard_listener.funcs.push_back([line_node, pos, size](auto event) {
            static float offset = 0.f;
            switch (event.getKeyCode())
            {
            case DisplayControl::EKeyCode::Key2:
                line_node->setVisible(!line_node->isVisible());
                break;
            case DisplayControl::EKeyCode::KeyM:
                offset += 0.005f;
                line_node->getMaterial().setCustomProperty(
                    "u_mask0_tex_mat", calcMaskUVPosition(pos + glm::vec2(offset, 0.f), size));
                break;
            case DisplayControl::EKeyCode::KeyK:
                offset -= 0.005f;
                line_node->getMaterial().setCustomProperty(
                    "u_mask0_tex_mat", calcMaskUVPosition(pos + glm::vec2(offset, 0.f), size));
                break;
            }
        });
    }

    /// right line
    {
        const auto color = Color::fromHex(0xff0000ff);
        const auto pos = glm::vec2{-1.47f, 50.f};
        const auto size = glm::vec2{0.4f, 50.f};

        auto dots_mat = create_dots_mat(color, "carpet/ldw");
        auto dots_mask_mesh = mesh_manager.createMesh(carpet_geom, dots_mat);

        auto line_node = Scene::CMeshNode::create(dots_mask_mesh);
        line_node->getMaterial().setCustomProperty("u_mask0_tex_mat", calcMaskUVPosition(pos, size));
        line_node->setOrder(-64);
        scene->addChild(line_node);

        keyboard_listener.funcs.push_back([line_node, pos, size](auto event) {
            static float offset = 0.f;
            switch (event.getKeyCode())
            {
            case DisplayControl::EKeyCode::Key3:
                line_node->setVisible(!line_node->isVisible());
                break;
            case DisplayControl::EKeyCode::KeyN:
                offset += 0.005f;
                line_node->getMaterial().setCustomProperty(
                    "u_mask0_tex_mat", calcMaskUVPosition(pos + glm::vec2(offset, 0.f), size));
                break;
            case DisplayControl::EKeyCode::KeyJ:
                offset -= 0.005f;
                line_node->getMaterial().setCustomProperty(
                    "u_mask0_tex_mat", calcMaskUVPosition(pos + glm::vec2(offset, 0.f), size));
                break;
            }
        });
    }

    return scene;
}

void runApplication(const std::string& graphic_config_file)
{
    GraphicSettings graph_settings = getDefaultGraphicSettings();
    if (!graphic_config_file.empty())
    {
        graph_settings = Graphic::parseGraphicSettings(graphic_config_file);
    }

    graph_settings.resource.filesystems.emplace_back(Resource::DirectoryFileSystemSettings{
        boost::filesystem::current_path().string() + "/resources"});
    graph_settings.resource.filesystems.emplace_back(
        Resource::DirectoryFileSystemSettings{c_dev_resources_path});

    auto display_control = DisplayControl::createInstance();
    auto surface = display_control->createSurface(c_wnd_size, c_surface_format);
    auto context = display_control->createContext(*surface);
    context->makeCurrent(*surface);

    DisplayControl::CWindowCloseListener closed_wnd_listener(surface->getWindowId());
    display_control->subscribeEventsListener(closed_wnd_listener);

    KeyboardListener keyboard_listener;
    display_control->subscribeEventsListener(keyboard_listener);

    auto graphic_root = createInstance(graph_settings);

    auto scene = createScene(*graphic_root, keyboard_listener);

    graphic_root->getGraphicContext().setViewport(Context::CViewArea(c_wnd_size));
    while (!closed_wnd_listener.isWindowClosed())
    {
        graphic_root->getGraphicContext().clearViewport();

        scene->render();

        display_control->processEvents();
        context->swapBuffers(*surface);
    }

    graphic_root.reset();
    context->release();
    context.reset();
    surface.reset();
    display_control.reset();
}

} // namespace

int main(int argc, const char** argv)
{
    namespace options = boost::program_options;

    std::string config_file;
    // clang-format off
    options::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Produce help message.")
        ("config,c",
             options::value<std::string>(&config_file),
             "Specify configuration file.")
        ;
    // clang-format on

    ELogger::Options logopts;
    ELogger::addOptions(desc, logopts);

    try
    {
        options::variables_map vm;
        options::store(options::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help"))
        {
            std::cout << desc << std::endl;
            return EXIT_SUCCESS;
        }

        options::notify(vm);
    }
    catch (const options::error& e)
    {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    ELogger::init(ELogger::Config(
        ELogger::AppInfo("TEST", "Test application"),
        ELogger::ContextInfo("CTA", "Carpet test application."),
        logopts));

    runApplication(config_file);

    return EXIT_SUCCESS;
}
