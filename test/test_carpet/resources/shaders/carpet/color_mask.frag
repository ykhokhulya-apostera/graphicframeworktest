#version 300 es
precision mediump float;

uniform sampler2D u_tex;

in vec4 v_color;
in vec2 v_tex0;

out vec4 out_frag_color;

void main()
{
    vec4 final_color = v_color;

    float v = texture(u_tex, v_tex0).r;
    final_color.rgb *= v;
    final_color.a = smoothstep(0., 0.2, v);

    out_frag_color = final_color;
}
