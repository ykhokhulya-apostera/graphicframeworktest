#version 300 es

in vec4 a_pos;
in vec2 a_tex0;

uniform mat4 u_mvp;
uniform float u_opacity;

uniform mat3 u_texTransform;
uniform vec4 u_color;

out vec4 v_color;
out vec2 v_tex0;

void main()
{
    gl_Position = u_mvp * a_pos;

    v_tex0 = (u_texTransform * vec3(a_tex0, 1.0)).xy;
    v_color = vec4(u_color.rgb, u_color.a * u_opacity);
}
