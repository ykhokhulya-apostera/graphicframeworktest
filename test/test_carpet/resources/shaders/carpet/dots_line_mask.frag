#version 300 es
precision mediump float;

uniform vec2 u_cell_size;
uniform float u_dot_size_coeff;

uniform sampler2D u_mask0;
uniform sampler2D u_mask1;

in vec2 v_tex;
in vec4 v_color;
in vec2 v_mask0_tex;
in vec2 v_mask1_tex;
in vec2 v_uv;
in float v_distance_shading;

out vec4 out_frag_color;

void main()
{
    float tail_dot_size = 1.0 - texture(u_mask0, v_mask0_tex).r;
    float solid_line_v = texture(u_mask1, v_mask1_tex).r;

    vec2 uv = v_tex;
    uv /= u_cell_size;
    uv = 2.0 * fract(uv) - 1.0;
    uv.y *= 1.0 - v_uv.y * v_uv.y;

    float e = 0.1 + v_uv.y;

    float alpha = smoothstep(0.0, 0.5, tail_dot_size) * (1.0 - v_distance_shading);
    float beta = smoothstep(e, -e, length(uv) - u_dot_size_coeff * tail_dot_size);
 
    vec4 solid_color = vec4(v_color.rgb * solid_line_v, smoothstep(0., 0.05, v_color.a * solid_line_v));
    vec4 dots_color = vec4(v_color.rgb * beta, v_color.a * alpha);

    out_frag_color = solid_color + step(0.95, 1.0 - solid_line_v) * dots_color;
}
