#version 300 es
in vec4 a_pos;
in vec4 a_color; // (u, v, distance shading, 1.0)
in vec2 a_tex0;

uniform mat4 u_mvp;
uniform vec4 u_color;
uniform float u_opacity;

uniform mat3 u_mask0_tex_mat;
uniform mat3 u_mask1_tex_mat;

out vec2 v_tex;
out vec4 v_color;
out vec2 v_mask0_tex;
out vec2 v_mask1_tex;
out vec2 v_uv;
out float v_distance_shading;

void main()
{
   gl_Position = u_mvp * a_pos;
   v_tex = a_tex0;
   v_mask0_tex = vec3(u_mask0_tex_mat * vec3(a_tex0, 1.0)).xy;
   v_mask1_tex = vec3(u_mask1_tex_mat * vec3(a_tex0, 1.0)).xy;
   v_color = vec4(u_color.rgb, u_color.a * u_opacity);
   v_uv = a_color.xy;
   v_distance_shading = a_color.z;
}
