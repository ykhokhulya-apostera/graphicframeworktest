#version 300 es
precision mediump float;

uniform vec2 u_cell_size;
uniform float u_dot_size_coeff;

uniform sampler2D u_mask0;

in vec2 v_tex0;
in vec4 v_color;
in vec2 v_mask0_tex;
in vec2 v_uv;
in float v_distance_shading;

out vec4 out_frag_color;

void main()
{
   float size = 1.0 - texture(u_mask0, v_mask0_tex).r;

   vec2 uv = v_tex0;
   uv /= u_cell_size;
   uv = 2.0 * fract(uv) - 1.0;
   uv.y *= 1.0 - v_uv.y * v_uv.y;

   float e = 0.1 + v_uv.y;

   float alpha = smoothstep(0.0, 0.5, size) * (1.0 - v_distance_shading);
   float beta =  smoothstep(e, -e, length(uv) - u_dot_size_coeff * size);

	out_frag_color = vec4(v_color.rgb * beta, v_color.a * alpha);
}
